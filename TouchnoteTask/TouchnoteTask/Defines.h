//
//  Defines.h
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#ifndef TouchnoteTask_Defines_h
#define TouchnoteTask_Defines_h


//API Definitions
#define kGitHubCommitsServiceUrl @"https://api.github.com/repos/rails/rails/commits?top=master"


#endif
