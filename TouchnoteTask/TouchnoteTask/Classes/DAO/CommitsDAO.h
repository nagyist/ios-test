//
//  CommitsDAO.h
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommitsDAO : NSObject

+ (id)sharedInstance;

//method to request the commits
-(void)requestCommitsWithSuccess:(void (^)(NSArray *commits))success failure:(void (^)(void))failure;

@end
