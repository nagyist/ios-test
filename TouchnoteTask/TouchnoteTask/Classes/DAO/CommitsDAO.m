//
//  CommitsDAO.m
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import "CommitsDAO.h"
#import "CommitModel.h"

@implementation CommitsDAO

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)requestCommitsWithSuccess:(void (^)(NSArray *))success failure:(void (^)(void))failure{
    
    
    //creates the url and the request
    NSURL *url = [NSURL URLWithString:kGitHubCommitsServiceUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
    
    //performs an asynchronously request
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               
                               //if everything is ok it will continue
                               if ([data length] >0 && error == nil)
                               {
                                   NSError *error1;
                                   NSArray *jsonCommitsArray = [NSJSONSerialization
                                                                      JSONObjectWithData:data options:kNilOptions error:&error1];
                                   
                                   
                                   if (jsonCommitsArray) {
                                       NSMutableArray *commitsArray = [@[] mutableCopy];
                                       
                                       //starts the parsing of the commits objects
                                       for (NSDictionary *commitDict in jsonCommitsArray) {
                                           CommitModel *commit = [[CommitModel alloc] initWithDictionary:commitDict];
                                           if (commit) {
                                               [commitsArray addObject:commit];
                                           }
                                       }
                                       
                                       //executes the success block
                                       if(success){
                                           success([NSArray arrayWithArray:commitsArray]);
                                       }
                                       
                                   }else{
                                       if (failure) {
                                           failure();
                                       }
                                   }
                                   
                               }
                               else if ([data length] == 0 && error == nil)
                               {
                                   if (failure) {
                                       failure();
                                   }
                                   NSLog(@">> Something wrong happend! Try again...");
                               }
                               else if (error != nil){
                                   NSLog(@"Error = %@", error);
                                   if (failure) {
                                       failure();
                                   }
                               }
                               
                              

                           }];


}

@end
