//
//  TouchNotesUtils.h
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TouchNotesUtils : NSObject

+ (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock;
+ (BOOL)checkInternetConnection;

@end
