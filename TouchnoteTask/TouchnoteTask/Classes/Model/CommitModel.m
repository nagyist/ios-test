//
//  CommitModel.m
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import "CommitModel.h"

@implementation CommitModel

- (id) initWithDictionary: (NSDictionary *) dictionary{
    
    self = [super init];
    if (self) {
        [self hydrateWithDictionary: dictionary];
    }
    return self;
}

- (void) hydrateWithDictionary: (NSDictionary *) dictionary
{
    //starts parsing the direct ones
    self.sha = dictionary[@"sha"]?:@"";
    self.message = dictionary[@"commit"][@"message"]?:@"";

    //parses the author
    NSDictionary *authorDict = dictionary[@"author"]?:nil;
    if (authorDict) {
        self.author = [[AuthorModel alloc] initWithDictionary:authorDict];
        self.author.realname = dictionary[@"commit"][@"author"][@"name"]; // it is outside of the expected dictionary.
    }
}




@end
