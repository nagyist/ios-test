//
//  CommitCellView.m
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import "CommitCellView.h"

@implementation CommitCellView

+ (NSString *)reuseIdentifier {
    return @"CommitCellView";
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureRow{
    
    [self.nameLabel setTextColor:[UIColor blackColor]];
    [self.nameLabel setFont:[UIFont boldSystemFontOfSize:15]];
    
}


-(void)hydrateWithCommits:(NSArray *)commits{
    
    float height = 0;
    
    //this will add a label with the commits
    for (NSString *commitMessage in commits) {
        
        UILabel *commitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,height, self.commitsView.frame.size.width, STANDARD_HEIGHT_OF_COMMIT_LABEL)];
        [commitLabel setFont:[UIFont systemFontOfSize:10]];
        [commitLabel setText:[NSString stringWithFormat:@"commit: %@", commitMessage]];
        [self.commitsView addSubview:commitLabel];
        height+=commitLabel.frame.size.height;
    }

}

@end
