//
//  CommitsViewController.h
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommitsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *commitsTableView; //the table used for show the commits

@end
