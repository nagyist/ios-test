//
//  TNTestTests.m
//  TNTestTests
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TNClient.h"
#import "TRVSMonitor.h"
#import "TNDataCache.h"

@interface TNTestTests : XCTestCase

@end

@implementation TNTestTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFetch
{
    
    __block TRVSMonitor *monitor = [TRVSMonitor monitor];
    __block NSError *err = nil;
    
    [[TNClient sharedClient] getCommitsByAuthorForProject:@"rails" success:^(NSArray *commits)
     {
        [monitor signal];
     }
                                    failure:^(NSError *error)
     {
         err = error;
         [monitor signal];
     }];
    
    [monitor wait];
    
    XCTAssertNil(err);
    XCTAssertNotEqual([TNDataCache sharedDataCache].commits.count,0);

}

@end
