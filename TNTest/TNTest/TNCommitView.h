//
//  TNCommitView.h
//  TNTest
//
//  Created by Ashley Thwaites on 19/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TNCommit;

@interface TNCommitView : UIView

@property (weak, nonatomic) IBOutlet UILabel *commitLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

+(id)viewWithCommit:(TNCommit*)commit;

@end
