//
//  TNCommitView.m
//  TNTest
//
//  Created by Ashley Thwaites on 19/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import "TNCommitView.h"
#import "TNCommit.h"

@implementation TNCommitView

+(id)viewWithCommit:(TNCommit*)commit
{
    static UINib *commitViewNib = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        commitViewNib = [UINib nibWithNibName:@"TNCommitView" bundle:nil];
    });
    
    
    if (commitViewNib)
    {
        NSArray *objects = [commitViewNib instantiateWithOwner:self options:nil];
        if ( (objects.count >0) && [objects[0] isKindOfClass:[TNCommitView class]])
        {
            TNCommitView * view = (TNCommitView*)objects[0];
            view.commitLabel.text = commit.sha;
            view.messageLabel.text = commit.message;
            return view;
        }
    }
    
    return nil;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
