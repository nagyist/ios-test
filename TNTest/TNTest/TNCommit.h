//
//  TNCommit.h
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TNAuthor;

@interface TNCommit : NSObject

@property (nonatomic, strong) TNAuthor *author;
@property (nonatomic, strong) NSString *sha;
@property (nonatomic, strong) NSString *message;

- (id)initWithDictionary:(NSDictionary*)dict;

@end
