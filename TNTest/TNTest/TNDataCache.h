//
//  TNDataCache.h
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TNAuthor;
@class TNCommit;

// this model could easily be swapped for coredata
// we dont yet know if core data is appropriate for this project

@interface TNDataCache : NSObject

+ (TNDataCache *)sharedDataCache;

@property (nonatomic, strong) NSDictionary *authors;
@property (nonatomic, strong) NSArray *commits;

-(void)updateWithJSON:(NSDictionary*)dictionary
                 success:(void(^)())success
                 failure:(void (^)(NSError *error))failure;

@end
