//
//  TNAuthorTableViewCell.h
//  TNTest
//
//  Created by Ashley Thwaites on 18/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TNAuthor;

@interface TNAuthorTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIView *commitsView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commitContentHeight;
-(void)updateWithAuthor:(TNAuthor*)author;

@end
