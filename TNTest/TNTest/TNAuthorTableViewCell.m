//
//  TNAuthorTableViewCell.m
//  TNTest
//
//  Created by Ashley Thwaites on 18/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import "TNAuthorTableViewCell.h"
#import "TNAuthor.h"
#import "TNCommit.h"
#import "TNCommitView.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIView+AutoLayout.h"

@implementation TNAuthorTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse
{    
    [self.commitsView removeConstraints:self.commitsView.constraints];
    [self.commitsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

-(void)updateWithAuthor:(TNAuthor*)author;
{
    self.nameLabel.text = author.name;
    [self.avatarImageView setImageWithURL:author.avatarURL usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    UIView *prevView = nil;
    for (TNCommit *commit in author.commits)
    {
        TNCommitView *view = [TNCommitView viewWithCommit:commit];
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [self.commitsView addSubview:view];

        if (prevView == nil)
        {
            [view pinToSuperviewEdges:(JRTViewPinLeftEdge | JRTViewPinTopEdge | JRTViewPinRightEdge)  inset:0];
        }
        else
        {
            [view pinToSuperviewEdges:(JRTViewPinLeftEdge | JRTViewPinRightEdge)  inset:0];
            [view pinEdge:NSLayoutAttributeTop toEdge:NSLayoutAttributeBottom ofItem:prevView];
        }
        prevView = view;
    }

    [prevView pinToSuperviewEdges:JRTViewPinBottomEdge inset:0];


//    self.commitContentHeight.constant = author.commits.count * 30.0f;
}

@end
