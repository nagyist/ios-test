//
//  TNAuthor.h
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TNAuthor : NSObject

@property (nonatomic, strong) NSNumber *guid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSURL *avatarURL;
@property (nonatomic, strong) NSMutableArray *commits;

- (id)initWithDictionary:(NSDictionary*)initWithDictionary;

@end
