//
//  TNClient.m
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import "TNClient.h"
#import "TNDataCache.h"

NSString* const TNClientErrorDomain = @"TNClientErrorDomain";

@implementation TNClient

+ (TNClient *)sharedClient {
    static TNClient *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate,
                  ^{
                      NSURL *url = [NSURL URLWithString:@"https://api.github.com"];
                      _sharedClient = [[[self class] alloc] initWithBaseURL:url];
                  });
    
    return _sharedClient;
}

-(id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self)
    {
//        self.responseSerializer = [[TNClientResponseSerializer alloc] init];
//        self.requestSerializer = [[TNClientRequestSerializer alloc] init];
    }
    return self;
}

-(void)getCommitsByAuthorForProject:(NSString*)projectName
              success:(void (^)(NSArray *commits))success
              failure:(void (^)(NSError *error))failure
{
    // rely on default pagination, should probably look at api docs more here
    // http://developer.github.com/v3/repos/commits/
    
    NSString *path = [NSString stringWithFormat:@"repos/%@/%@/commits",projectName,projectName];
    
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         // lets just make sure its at least an array of something
         if ([responseObject isKindOfClass:[NSArray class]])
         {
             [[TNDataCache sharedDataCache] updateWithJSON:responseObject success:^()
              {
                 if (success)
                 {
                     success(nil);
                 }
              }
                failure:failure];
         }
         else
         {
             if (failure)
             {
                 NSError *error = [[NSError alloc] initWithDomain:TNClientErrorDomain code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Could not validate",NSLocalizedDescriptionKey,nil]];
                 failure(error);
             }
         }
     } failure:^(NSURLSessionDataTask *task, NSError *error) {
         if (failure)
         {
             failure(error);
         }
     }];
}


@end
