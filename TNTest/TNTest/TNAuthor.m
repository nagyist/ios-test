//
//  TNAuthor.m
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import "TNAuthor.h"
#import "NSObject+JSONhelper.h"
#import "NSDictionary+JSONhelper.h"

@implementation TNAuthor

- (id)initWithDictionary:(NSDictionary*)dict
{
    if ((self = [super init]) != nil)
    {
        NSDictionary *authorDict = [dict dictionaryForKey:@"author"];
        if (authorDict)
        {
            [self setValueFromDictionary:authorDict withKey:@"id" forKey:@"guid" withDefault:nil];
            [self setValueFromDictionary:authorDict withKey:@"avatar_url" forKey:@"avatarURL" withDefault:nil];
        }
        
        // the name of the author is inside the short author child node beneath the commit.
        NSDictionary *commitDict = [dict dictionaryForKey:@"commit"];
        if (commitDict)
        {
            NSDictionary *authorDict = [commitDict dictionaryForKey:@"author"];
            if (authorDict)
            {
                [self setValueFromDictionary:authorDict withKey:@"name" forKey:@"name" withDefault:nil];
            }
        }
        
        self.commits = [[NSMutableArray alloc] initWithCapacity:10];
    }
    return self;
}

@end
