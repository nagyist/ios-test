//
//  TNDataCache.m
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import "TNDataCache.h"
#import "TNCommit.h"
#import "TNAuthor.h"

// The structure of the data here and the associations make a real case for usiung core data.
// MagicalRecord makes dealing with coredata supper simple, and in this case an in memory store would be a good option
// Magical record would allow us to really easily fetch the data frpm the cache in any way we see fit.

// I am implementing this as a hard coded NSObject store becuase the project requirements only require 1 method to search and sort the data.
// If we were building a real app we would have broader project requirements which probably would make core data the right choice.


NSString* const TNDataCacheErrorDomain = @"TNDataCacheErrorDomain";
NSString* const TNDataCacheValidateError = @"Could not validate";

@implementation TNDataCache

+ (TNDataCache *)sharedDataCache {
    static TNDataCache *_sharedDataCache = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate,
                  ^{
                      _sharedDataCache = [[[self class] alloc] init];
                  });
    
    return _sharedDataCache;
}

-(void)updateWithJSON:(id)json
                    success:(void(^)())success
                    failure:(void (^)(NSError *error))failure;
{

    
    if ([json isKindOfClass:[NSArray class]])
    {
        // we have an array of objects which should be commits
        NSMutableArray *newCommits = [[NSMutableArray alloc] initWithCapacity:[json count]];
        NSMutableDictionary *newAuthors = [[NSMutableDictionary alloc] initWithCapacity:[json count]];
        for (NSDictionary *dict in json)
        {
            // parse the objects and do some very light validation and create associations
            TNAuthor *author = [[TNAuthor alloc] initWithDictionary:dict];
            TNCommit *commit = [[TNCommit alloc] initWithDictionary:dict];
            
            if (author.guid && commit.sha)
            {
                TNAuthor *existingAuthor = [newAuthors objectForKey:author.guid];
                if (existingAuthor)
                {
                    [existingAuthor.commits addObject:commit];
                }
                else
                {
                    [author.commits addObject:commit];
                    [newAuthors setObject:author forKey:author.guid];
                }
                [newCommits addObject:commit];
            }
            else
            {
                if (failure)
                {
                    NSError *error = [[NSError alloc] initWithDomain:TNDataCacheErrorDomain code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:TNDataCacheValidateError,NSLocalizedDescriptionKey,nil]];
                    failure(error);
                }
            }
        }
        self.commits = newCommits;
        self.authors = newAuthors;

        if (success)
        {
            success();
        }
        return;
    }

    // pass a validation error if we have a failure block
    if (failure)
    {
        NSError *error = [[NSError alloc] initWithDomain:TNDataCacheErrorDomain code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Could not validate",NSLocalizedDescriptionKey,nil]];
        failure(error);
    }

}

@end
