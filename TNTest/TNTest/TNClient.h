//
//  TNClient.h
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface TNClient : AFHTTPSessionManager

+ (TNClient *)sharedClient;

-(void)getCommitsByAuthorForProject:(NSString*)projectName
                    success:(void (^)(NSArray *authors))success
                    failure:(void (^)(NSError *error))failure;

@end
