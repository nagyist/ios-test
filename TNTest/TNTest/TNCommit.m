//
//  TNCommit.m
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//


#import "TNCommit.h"
#import "TNDataCache.h"
#import "NSObject+JSONhelper.h"
#import "NSDictionary+JSONhelper.h"

@implementation TNCommit

- (id)initWithDictionary:(NSDictionary*)dict
{
    if ((self = [super init]) != nil)
    {
        [self setValueFromDictionary:dict withKey:@"sha" forKey:@"sha" withDefault:nil];
        NSDictionary *commitDict = [dict dictionaryForKey:@"commit"];
        if (commitDict)
        {
            [self setValueFromDictionary:commitDict withKey:@"message" forKey:@"message" withDefault:nil];
        }
    }
    return self;
}


@end
