    //
//  ViewController.m
//  TNTest
//
//  Created by Ashley Thwaites on 17/01/2014.
//  Copyright (c) 2014 Toolbox. All rights reserved.
//

#import "TNCommitsByAuthorViewController.h"
#import "TNClient.h"
#import "TNDataCache.h"
#import "TNAuthorTableViewCell.h"

@interface TNCommitsByAuthorViewController ()
{
    NSArray *authors;
    TNAuthorTableViewCell *cellForSizeing;
}

@end

@implementation TNCommitsByAuthorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    cellForSizeing = [self.tableView dequeueReusableCellWithIdentifier:@"TNAuthorTableViewCell"];
    
    [self refreshControlChanged:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return authors.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TNAuthorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TNAuthorTableViewCell"];
    
    [cell updateWithAuthor:authors[indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cellForSizeing prepareForReuse];
    cellForSizeing.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.bounds), CGRectGetHeight(cellForSizeing.bounds));

    [cellForSizeing updateWithAuthor:authors[indexPath.row]];


    [cellForSizeing setNeedsLayout];
    [cellForSizeing layoutIfNeeded];

    return [cellForSizeing.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200.0f;
}


- (IBAction)refreshControlChanged:(id)sender {
    [self.refreshControl beginRefreshing];
    [[TNClient sharedClient] getCommitsByAuthorForProject:@"rails" success:^(NSArray *commits)
     {
         // self reload table
         NSLog(@"Got data");
         authors = [[TNDataCache sharedDataCache].authors allValues];
         
         // sort the authors?
         [self.tableView reloadData];
         [self.refreshControl endRefreshing];
     }
                                                  failure:^(NSError *error)
     {
         [self.refreshControl endRefreshing];
         if (error)
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                             message:[error localizedDescription]
                                                            delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];
         }
     }];
}
@end
